#[macro_use]
extern crate serde;
extern crate serde_yaml;

use serde::{Serialize, Deserialize};

use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

// #[derive(Serialize, Deserialize, Debug, Hash, PartialEq, Eq)]
// pub enum Method {
//     Get,
//     Post,
//     Patch,
//     Put,
//     Delete
// }
pub type Method = String;

#[derive(Serialize, Deserialize, Debug)]
struct Swagger {
    openapi: String,
    info: Info,
    #[serde(skip_serializing_if="Option::is_none")]
    servers: Option<Vec<Server>>,
    paths: HashMap<String, HashMap<Method, Endpoint>>
}

#[derive(Serialize, Deserialize, Debug)]
struct Info {
    title: String,
    description: String,
    version: String
}

#[derive(Serialize, Deserialize, Debug)]
struct Server{
    url: String,
    description: String
}

#[derive(Serialize, Deserialize, Debug)]
struct Endpoint {
    summary: String,
    #[serde(skip_serializing_if="Option::is_none")]
    description: Option<String>,
    responses: HashMap<ResponseCode, Response>,
    #[serde(skip_serializing_if="Option::is_none")]
    parameters: Option<Vec<Param>>
}

#[derive(Serialize, Deserialize, Debug)]
struct Param {
    name: String,
    r#in: String,
    #[serde(skip_serializing_if="Option::is_none")]
    required: Option<bool>,
    description: Option<String>
}

pub type ResponseCode = String;
pub type ContentType = String;

#[derive(Serialize, Deserialize, Debug)]
struct Response {
    description: String,
    #[serde(skip_serializing_if="Option::is_none")]
    content: Option<HashMap<ContentType, serde_yaml::Value>>
}

fn gen_endpoint_function(path: &str, swagger: &Swagger, writer: &mut impl Write) {
    let resource = "User";

    for (method, endpoint) in swagger.paths.get(path).unwrap().iter() {
        // Todo
        // Loop through all methods

        let functionName = path.split('/').fold(String::from(method), |mut url, ref path_piece| {
            if (path_piece.is_empty()) {
                return url;
            }

            if &path_piece[0..1] == "{" {
                return url;    
            }

            url.push('_');

            // let capitalized = path_piece.get(0..1).unwrap().to_uppercase();
            url.push_str(path_piece);

            url
        });

        let content_type = "application/json";
        let mut args = String::new();

        if let Some(params) = &endpoint.parameters {
            params.iter().for_each(|param| {
                if param.r#in == "path" {
                    args.push_str(&param.name);
                    args.push(',');
                }
            });
        };

        args.push_str("body");

        let endpoint_function = format!("
export async function {signature}({args}) {{
    return fetch(`{url}`, {{
        method: '{method}',
        headers: {{
            'Content-Type': '{content_type}'
        }},
        body: JSON.stringify(body)
    }})
}}", 
            signature = functionName,
            args = args,
            method = method,
            url = path,
            content_type = content_type,
        );

        write!(writer, "{}", endpoint_function);
    }
}

fn gen_api_client(filename: &str, swagger: Swagger) -> std::io::Result<()> {
    // Make the file
    let mut client_api = File::create(&format!("{}.js", filename))?;

    swagger.paths.iter().for_each(|(path, _)| {
        gen_endpoint_function(path, &swagger, &mut client_api);
    });

    Ok(())
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("docuvisory.yaml")?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer);

    let swagger: Swagger = serde_yaml::from_str(&buffer).unwrap();

    gen_api_client("docuvisory", swagger);

    Ok(())
}
